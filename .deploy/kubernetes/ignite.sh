#! /bin/bash
set -xe
kind create cluster --name exercise-02-cluster --wait 10m --config ./kind.yml
helm install nfs-server stable/nfs-server-provisioner
kubectl apply --filename https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=300s
kubectl create -f ./ns.yml
#kubectl apply -f pg-secret.yaml -n exercise-02
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: pg-password
  namespace: exercise-02
type: Opaque
data:
  POSTGRES_PASSWORD: $(echo -n "worker" | base64 -w0)
EOF
kubectl apply -f postgresql.yml -n exercise-02
kubectl apply -f application.yaml -n exercise-02
helm install --create-namespace -n portainer portainer portainer/portainer \
    --set service.type=ClusterIP \
    --set ingress.enabled=true \
    --set ingress.ingressClassName=nginx \
    --set ingress.hosts[0].host=hello.port.com \
    --set ingress.hosts[0].paths[0].path="/"
